# pure-scripts

Scripts for managing Pure FlashArray

### multipath-alias.py
Script to generate a multipath.conf multipaths configuration to set multipath
aliases for each wwid to the Volume name on the Pure FlashArray.
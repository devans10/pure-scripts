#/usr/bin/python
from __future__ import print_function

## Output multipath.conf mulitpath aliases that name the wwids after
## the Volume names on the Pure Storage Array.

import os
import sys
import json
import glob
from purestorage import FlashArray


array = FlashArray("flasharry.example.com", "pureuser", "pureuser")
volumes = array.list_volumes()

wwids = []
with open("/etc/multipath/wwids", 'r') as f:
    for line in f:
        alias = ""
        if line.startswith("#"):
            continue
        else:
            wwid = line.rstrip().strip('/')
            for vol in volumes:
                if wwid.upper().endswith(vol['serial']):
                    alias = vol['name']
                    break
            if alias != "":
                wwids.append({"wwid": wwid, "alias": alias})
                
print("multipaths {")
for wwid in wwids:
    print("    multipath {")
    print("        wwid    %s" % wwid['wwid'])
    print("        alias   %s" % wwid['alias'])
    print("    }")

print("}")